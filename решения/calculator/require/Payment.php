<?php


/**
 * Модель платежа
 */

class Payment
{
    //дата платежа
    const FIELD_DATE = 'date';

    //хэш
    const FIELD_HASH = 'hash';

    //остаток долга
    const FIELD_DEBT = 'debt';

    //общая сумма
    const FIELD_PAYMENT = 'payment';

    //погашение процентов
    const FIELD_PAYMENT_PERCENTS = 'payment_percents';

    //погашение основного долга
    const FIELD_PAYMENT_BODY = 'payment_body';

    //данные платежа
    private $data = array();

    //таблица db
    private $table = 'payments';


    public function __construct()
    {
        $this->data = array(
            self::FIELD_HASH => '',
            self::FIELD_DATE => '',
            self::FIELD_DEBT => 0,
            self::FIELD_PAYMENT => 0,
            self::FIELD_PAYMENT_PERCENTS => 0,
            self::FIELD_PAYMENT_BODY => 0
        );
    }

    /**
     * сеттер
     *
     * @param string $field ключ
     * @param mixed $data значение
     */
    public function set($field, $data)
    {
        if (isset($this->data[$field])) {
            $this->data[$field] = $data;
        }
    }

    /**
     * геттер
     *
     * @param string $field ключ
     * @return mixed значение
     */
    public function get($field, $format = false)
    {
        if (isset($this->data[$field])) {
            if ($format == 'currency') {
                return round($this->data[$field], 2);
            } elseif ($format == 'date') {
                return $this->data[$field]->format('Y-m-d');
            }

            return $this->data[$field];
        }
        return null;
    }


    public function save()
    {
        $stmt = mysqlConnect::connect()->prepare('INSERT INTO ' . $this->table . ' (hash, date, debt, payment, payment_percents, payment_body) VALUES (?,?,?,?,?,?)');
        $stmt->bind_param("ssdddd"
            , $this->get(self::FIELD_HASH)
            , $this->get(self::FIELD_DATE)
            , $this->get(self::FIELD_DEBT)
            , $this->get(self::FIELD_PAYMENT)
            , $this->get(self::FIELD_PAYMENT_PERCENTS)
            , $this->get(self::FIELD_PAYMENT_BODY)
        );
        $stmt->execute();
    }

}
