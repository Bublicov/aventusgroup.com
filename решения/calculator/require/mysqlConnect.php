<?php

/**
 * Коннект к MySQL
 */
class mysqlConnect
{
    protected static $connect;


    /**
     * Коннект к MySQL
     */
    public static function connect()
    {
        if (null === static::$connect) {
            static::$connect = new mysqli('localhost', MYSQL_USER, MYSQL_PASS, MYSQL_DBNAME);

            if (!empty(static::$connect->connect_error)) {
                throw new Exception('MySQL: ' . static::$connect->connect_error);
            }
        }
        return static::$connect;
    }
}