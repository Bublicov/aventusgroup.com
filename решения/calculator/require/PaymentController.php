<?php

/**
 * Расчёт аннуитентных платежей
 */

class PaymentController
{
    /**
     * @var float сумма кредита
     */
    private $amount = null;

    /**
     * @var int срок кредита в месяцах
     */
    private $period = null;

    /**
     * @var float процентная ставка по кредиту
     */
    private $rate = null;

    /**
     * @var date дата первого платежа
     */
    private $date = null;

    /**
     * @var float выплачено
     */
    private $current_paid = null;

    /**
     * @var date дата текущего платежа
     */
    private $current_date = null;

    /**
     * @var mixed расчитанные платежи
     */
    private $payments = [];


    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function setPeriod($period)
    {
        $this->period = $period;
    }

    public function setRate($rate)
    {
        $this->rate = $rate / 100;
    }

    public function setDate($date, $format)
    {
        $this->date = DateTime::createFromFormat($format, $date);
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getPeriod()
    {
        return $this->period;
    }

    public function getRate()
    {
        return (!empty($this->rate) ? $this->rate * 100 : null);
    }

    public function getDate($format)
    {
        return (!empty($this->date) ? $this->date->format($format) : null);
    }

    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * генерация хэша
     * @param string $salt соль
     * @return string ключ
     */
    public function getHash($salt)
    {
        $date = $this->date->format('Y-m-d');
        return sha1("$salt $this->amount $this->period $this->rate $date");
    }

    /**
     * расчет аннуитетного платежа
     *
     * @return float чистый аннуитет
     */
    public function calcPaymentValue()
    {
        $rate_month = $this->rate * 1 / 12;
        return ($rate_month / (1 - pow(1 + $rate_month, -1 * $this->period))) * $this->amount;
    }

    public function calcDebt($payment_body)
    {
        $debt = $this->amount - $this->current_paid - $payment_body;
        $this->current_paid += $payment_body;
        return ($debt < 0 ? 0 : $debt);
    }

    /**
     * Расчитать данные по платежам
     */
    public function calcAnnuityPayments()
    {
        $annuityPayment = $this->calcPaymentValue();

        foreach (range(1, $this->period) as $k) {

            if ($k === 1) {
                // первый месяц зависит от аннуитента
                $payment_body = $annuityPayment / pow((1 + $this->rate * 1 / 12), $this->period);
                $this->current_date = clone $this->date;
            } else {
                // не первый месяц зависит от платежа за прошлый месяц
                $payment_body = (1 + $this->rate * 1 / 12) * $this->payments[$k - 1]->get(Payment::FIELD_PAYMENT_BODY);
                $this->current_date->add(new DateInterval('P1M'));
            }
            $payment = new Payment();
            $payment->set(Payment::FIELD_HASH, $this->getHash(__METHOD__));
            $payment->set(Payment::FIELD_DATE, $this->current_date->format('Y-m-d'));
            $payment->set(Payment::FIELD_DEBT, $this->calcDebt($payment_body));
            $payment->set(Payment::FIELD_PAYMENT, $annuityPayment);
            $payment->set(Payment::FIELD_PAYMENT_PERCENTS, $annuityPayment - $payment_body);
            $payment->set(Payment::FIELD_PAYMENT_BODY, $payment_body);
            $payment->save();

            $this->payments[$k] = $payment;
        }
    }


    /**
     * итоговые затраты по указанному полю
     *
     * @param string $field название поля
     * @return float
     */
    public function calcTotal($field, $format)
    {
        $value = 0;
        foreach ($this->getPayments() as $payment) {
            $value += $payment->get($field, $format);
        }
        return $value;
    }

}
