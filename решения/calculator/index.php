<?php

/*
 * Написать скрипт кредитного калькулятора используя PHP и Javascript (можно любые сторонние библиотеки):
 * Клиент вводит сумму кредита, указывает срок кредита в месяцах и процентную ставку, дату первого платежа.
 * Скрипт выводит таблицу с графиком платежей в формате: № платежа, Дата платежа, Основной долг, проценты, общая сумма, остаток долга
 * Вид платежа - аннуитетный.
 * История подсчетов сохраняет в MySQL
 *
 * Время: 7 часов
 * */

require_once(__DIR__ . '/require/mysqlConnect.php');
require_once(__DIR__ . '/require/Payment.php');
require_once(__DIR__ . '/require/PaymentController.php');

//config
define("MYSQL_DBNAME", 'test');
define("MYSQL_USER", 'root');
define("MYSQL_PASS", '');

error_reporting(E_ERROR | E_PARSE);



$payments = new PaymentController();

if (!empty($_POST)) {

    $payments->setAmount($_POST['amount']);
    $payments->setPeriod($_POST['period']);
    $payments->setRate($_POST['rate']);
    $payments->setDate($_POST['datetimepicker'], 'd/m/Y');

    try {
        $payments->calcAnnuityPayments();
    } catch (Exception $e) {
        echo $e->getMessage();
        exit;
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Кредитный калькулятор</title>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>

    <style>
        .form-group {
            padding: 5px 0 5px 0;
        }

        .btn-submit {
            margin-top: 50px;
        }

        input.form-control {
            text-align: right;
            padding-right: 15px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>

</head>
<body>


<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h1>Кредитный калькулятор аннуитентных платежей</h1>
            <hr style="height:1px;border:none;color:#333;background-color:#CCCCCC;"/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <form action="" method="POST" role="form" data-toggle="validator">
                <div class="form-group">
                    <label for="amount">Сумма кредита</label>
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-usd"></span></span>
                        <input type="number" min="0" step="0.01" data-number-to-fixed="2"
                               value="<?= $payments->getAmount() ?>"
                               data-number-stepfactor="100" class="form-control" id="currency" name="amount" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="period">Срок кредита(в месяцах)</label>
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-signal"></span></span>
                        <input type="number" min="1" step="1" class="form-control" value="<?= $payments->getPeriod() ?>"
                               id="period" name="period" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="rate">Процентная ставка</label>
                    <div class="input-group">
                        <span class="input-group-addon">%</span>
                        <input type="number" min="0" step="0.01" data-number-to-fixed="2"
                               value="<?= $payments->getRate() ?>"
                               data-number-stepfactor="100" class="form-control" id="rate" name="rate" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="datetimepicker">Дата первого платежа</label>
                    <div class='input-group date'>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        <input type='text' class="form-control" id='datetimepicker' name='datetimepicker'
                               value="<?= $payments->getDate('d/m/Y') ?>" required/>
                    </div>
                </div>

                <button type="submit" class="btn btn-info btn-block btn-submit">Submit</button>
            </form>
        </div>

        <div class="col-md-9">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th># платежа</th>
                    <th>дата платежа</th>
                    <th>основной долг</th>
                    <th>проценты</th>
                    <th>общая сумма</th>
                    <th>остаток долга</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($payments->getPayments())) {
                    foreach ($payments->getPayments() as $key => $payment) {
                        $row .= '<tr><th scope="row">' . $key . '</th>'
                            . '<td>' . $payment->get(Payment::FIELD_DATE) . '</td>'
                            . '<td>' . $payment->get(Payment::FIELD_PAYMENT_BODY, 'currency') . '</td>'
                            . '<td>' . $payment->get(Payment::FIELD_PAYMENT_PERCENTS, 'currency') . '</td>'
                            . '<td>' . $payment->get(Payment::FIELD_PAYMENT, 'currency') . '</td>'
                            . '<td>' . $payment->get(Payment::FIELD_DEBT, 'currency') . '</td>'
                            . '</tr>';
                    }
                    echo $row;

                    $row_total .= '<tr class="warning"><th>Итого:</th>'
                        . '<td></td>'
                        . '<td>' . $payments->calcTotal(Payment::FIELD_PAYMENT_BODY, 'currency') . '</td>'
                        . '<td>' . $payments->calcTotal(Payment::FIELD_PAYMENT_PERCENTS, 'currency') . '</td>'
                        . '<td>' . $payments->calcTotal(Payment::FIELD_PAYMENT, 'currency') . '</td>'
                        . '<td></td>'
                        . '</tr>';
                    echo $row_total;
                }
                ?>
                </tbody>
            </table>
        </div>


    </div>
</div>
</body>
</html>