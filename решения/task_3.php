<?php

/*
 * Задание Nr.3.
 * В карьере А есть куча камней (сгенерированный массив), вес которых - от 1 килограмма до 20 тонн.
 * Есть грузовик, который может перевезти за один раз 30 тонн.
 * Грузовик должен перевезти все камни из карьера А в карьер Б.
 *
 * Надо предоставить PHP скрипт, который поможет грузовику перевезти все камни из карьера А в карьер Б
 * за минимальное количество поездок. Учитывать только вес камней - объем не имеет значения.
 */

/*
* Комментарий по реализации:
* Минимальное количество поездок получится если отсортировать камни по убыванию веса и разбирать их
* начиная с самых тяжёлых.
* Это можно представить как массив или как кучу(heap), в данном случае используется heap с сортировкой
* по убыванию
*
* Время: 1,5 часа
*/

class Career extends SplMaxHeap
{
    public $truck_volume = 30000;
    public $limit = 0;
    public $rounds = [1 => []];

    public function __construct($limit = 20)
    {
        $this->limit = $limit;

        for ($i = 0; $i < $this->limit; $i++) {
            $this->insert(rand(1, 20000));
        }
    }

    // расчитать рейсы
    public function calculateRounds()
    {
        while ($this->valid()) {
            foreach ($this->rounds as $key => $weight) {
                if (($this->truck_volume - array_sum($weight)) >= $this->current()) {
                    $this->rounds[$key][] = $this->current();
                    break;
                }
                if ($key == count($this->rounds)) {
                    $this->rounds[++$key][] = $this->current();
                }
            }
            $this->next();
        }
    }

    // получить элементы одной строкой
    public function getItemsAsString(){
        $str = '';
        while ($this->valid()) {
            $str .= ($str ? ', ':'') . $this->current();
            $this->next();
        }
        return $str;
    }

    // количество рейсов
    public function getCountRounds()
    {
        return count($this->rounds);
    }

    // список рейсов с загрузкой
    public function getRounds()
    {
        return $this->rounds;
    }

}

$career = new Career();
$career_heap = clone $career;

$career->calculateRounds();

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Рейсы из карьера А в карьер Б</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>


<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h1>Рейсы из карьера А в карьер Б</h1>
            <hr style="height:1px;border:none;color:#333;background-color:#CCCCCC;"/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <ul class="list-group">
                <li class="list-group-item">Грузоподъёмность: <?= $career->truck_volume ?></li>
                <li class="list-group-item">Количество камней: <?= $career->limit ?></li>
                <li class="list-group-item">Количество рейсов: <?= $career->getCountRounds() ?></li>
            </ul>
        </div>

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Куча камней: <?= $career_heap->getItemsAsString() ?></p>
                    <p>* массив отсортирован по убыванию, начинаем разбирать с самых тяжёлых пока не получим максимальную загрузку на рейс</p>
                </div>
                <!-- Default panel contents -->
                <div class="panel-heading">Рейсы камаза с загрузкой</div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Перевозимые камни</th>
                        <th>Масса камней</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                        $row = '';
                        foreach ($career->getRounds() as $key => $round){
                        $row .= '<tr><th scope=\'row\'>' . $key . '</th><td>' . implode(", ", $round) . '</td><td>' . array_sum($round) . '</td></tr>';
                    }
                    echo $row;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</div>
</body>
</html>