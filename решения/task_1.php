<?php

/*
 * Задание Nr.1. 
 * Написать PHP скрипт, с помощью которого можно сгенерировать шахматную доску.
 * 
 * Время: 1 час
*/


$board = range(1, 8);

foreach ($board as $key => $row) {
    $row = '';
    for ($i = 1; $i <= count($board); $i++) {
        if (empty($key % 2)) {
            $color = (empty($i % 2) ? 'black' : 'white');
        } else {
            $color = (empty($i % 2) ? 'white' : 'black');
        }
        $row .= '<td style="background-color: ' . $color . '; width: 50px; height: 50px"></td>';
    }
    //борда
    $table .= '<tr>' . $row . '</td>';

    //окантовка
    $row_h .= '<td style="background-color: white; width: 50px; height: 50px" valign="middle" align="center"> ' . chr($key + 97) . '</td>';
    $row_v .= '<tr><td style="background-color: white; width: 50px; height: 50px" valign="middle" align="center">' . (count($board) - $key) . '</td></tr>';
}

//вывод
echo '<table style="border: 1px solid black; border-collapse: collapse;">
        <tr>
            <td style="width: 50px; height: 50px"></td>
            ' . $row_h . '
            <td style="width: 50px; height: 50px"></td>
        </tr>
        <tr>
            <td style="width: 50px"><table>' . $row_v . '</table></td>
            <td colspan="' . count($board) . '">' . '<table style="border: 1px solid black; border-collapse: collapse;">' . $table . '</table>' . '</td>
            <td style="width: 50px"><table>' . $row_v . '</table></td>
        </tr>
        <tr>
            <td style="width: 50px; height: 50px"></td>
            ' . $row_h . '
            <td style="width: 50px; height: 50px"></td>
        </tr>
    </table>';