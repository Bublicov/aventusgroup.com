﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.2.53.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 26.04.2017 12:10:06
-- Версия сервера: 5.5.5-10.1.19-MariaDB
-- Версия клиента: 4.1
--


-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

-- 
-- Установка базы данных по умолчанию
--
USE test;

--
-- Описание для таблицы auto_history
--
DROP TABLE IF EXISTS auto_history;
CREATE TABLE auto_history (
  x INT(11) UNSIGNED NOT NULL,
  y INT(11) UNSIGNED NOT NULL,
  auto VARCHAR(255) NOT NULL
)
ENGINE = INNODB
AVG_ROW_LENGTH = 1820
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы payments
--
DROP TABLE IF EXISTS payments;
CREATE TABLE payments (
  id INT(11) NOT NULL AUTO_INCREMENT,
  hash VARCHAR(255) NOT NULL,
  date DATE DEFAULT NULL,
  debt DECIMAL(10, 4) DEFAULT NULL,
  payment DECIMAL(10, 4) DEFAULT NULL,
  payment_percents DECIMAL(10, 4) DEFAULT NULL,
  payment_body DECIMAL(10, 4) DEFAULT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci;

-- 
-- Вывод данных для таблицы auto_history
--
INSERT INTO auto_history VALUES
(1, 2, 'A'),
(5, 3, 'B'),
(4, 6, 'C'),
(1, 2, 'D'),
(7, 7, 'E'),
(4, 6, 'F'),
(5, 3, 'B'),
(9, 4, 'H'),
(4, 6, 'F');

-- 
-- Вывод данных для таблицы payments
--

-- Таблица test.payments не содержит данных

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;